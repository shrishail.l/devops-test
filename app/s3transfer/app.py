import os

from flask import Flask, render_template, request, redirect, send_file, url_for
from flask_restful import Resource, Api
from s3 import list_files, download_file, upload_file


app = Flask(__name__)
api = Api(app)

UPLOAD_FOLDER = "uploads"

BUCKET = "s3uploadtesting1"


@app.route('/')
def entry_point():
    return 'Hello World!'

@app.route("/storage")
def storage():
    contents = list_files("s3uploadtesting1")
    return render_template('storage.html', contents=contents)


@app.route("/upload", methods=['POST'])
def upload():
    if request.method == "POST":
        f = request.files['file']
        f.save(f.filename)
        upload_file(f"{f.filename}", BUCKET)

        return redirect("/storage")


@app.route("/download/<filename>", methods=['GET'])
def download(filename):
    if request.method == 'GET':
        output = download_file(filename, BUCKET)

        return send_file(output, as_attachment=True)




if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
