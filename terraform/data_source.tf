data "aws_availability_zones" "zones" {}

data "aws_ami" "amazon_linux" {
  owners      = ["amazon"]
  most_recent = true

  filter {
    name = "name"

    values = [
      "amzn2-ami-*-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

data "aws_ami" "amazon_linux_web" {
  owners      = ["763504679483"]
  most_recent = true

  filter {
    name = "name"

    values = [
      "recruitment",
    ]
  }
}

data "aws_vpc" "selected" {
tags = {
Name = "${var.vpc_name}"
}
}

data "aws_subnet" "vpc-private-eu-west-1a" {
  vpc_id = "${module.vpc.vpc_id}"
filter {
    name = "tag:Name"
    values = ["*-private-eu-west-1a"]
  }
}

data "aws_subnet" "vpc-public-eu-west-1a" {
  vpc_id = "${module.vpc.vpc_id}"
filter {
    name = "tag:Name"
    values = ["*-public-eu-west-1a"]
  }
}


