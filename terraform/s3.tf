resource "aws_s3_bucket" "s3upload_s3_bucket_test" {
  bucket = "s3uploadtesting1"
  acl = "private"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "POST"]
    allowed_origins = ["https://*"]
    max_age_seconds = 86400
  }

  versioning {
    enabled = false
  }

  tags = {
    Name = "s3upload.test"
    Environment = "dev"
  }
}

