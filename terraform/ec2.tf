resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = var.instance_type
 tags = {
    Name = "Bastion"
    env = "dev"
    author = "shrishail.l"
  }

subnet_id = "${data.aws_subnet.vpc-private-eu-west-1a.id}"
security_groups = [ "${aws_security_group.bastion.id}" ]
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.amazon_linux_web.id
  instance_type = var.instance_type
 tags = {
   Name = "web"
    env = "dev"
    author = "shrishail.l"
  }
subnet_id = "${data.aws_subnet.vpc-public-eu-west-1a.id}"
security_groups = [ "${aws_security_group.packer_build.id}" ]

}
